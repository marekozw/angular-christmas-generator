define(['ngapp'], function (ngapp) {
	ngapp.directive('addItemBox', function ($timeout) {
	    return {
	        restrict: 'E',
            scope: {
                onAdd: '&',
                clearAfter: '@',
                placeholder: '@',
                type: '@',
                minLength: '@',
                maxLength: '@',
                errorMessage: '@'
            },
	        templateUrl: 'templates/addItemBox.html',
            link: function (scope, element, attributes) {
                var input = element.find('input')[0];
                scope.$watch('input.length', function () {
                    if (input.length === 0) {
                        scope.setPristine();
                    }
                });
                scope.setPristine = function () {
                    scope.addForm.input.$setPristine();
                };
                scope.setFocus = function () {
                    $timeout(function () {
                        input.focus();
                    });
                };

                scope.triggerOnAddEvent = function () {
                    if (scope.newItem && scope.addForm.$valid) {
                        scope.onAdd({
                            item: scope.newItem
                        });

                        if (!scope.clearAfter) {
                            scope.clearAfter = 'true';
                        }
                        if (scope.clearAfter === 'true') {
                            scope.newItem = '';
                        }
                        scope.setPristine();
                    }
                    scope.setFocus();
                };
                scope.onKeyPress = function (event) {
                    if (event.which === 13) {
                        scope.triggerOnAddEvent();
                    }
                };
            }
	    };
	});
});
