<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('singlepage');
});

Route::post('/sendMails', function ()
{
    $data = ['target' => Input::json()->all()['target']];

    return Mail::queue('emails.welcome', $data, function ($message) {
        $message->to(Input::json()->all()['email'])
                ->subject('Christmas Generator');

    });
});
