define([
    'controllers/mainCtrl',

    'directives/addItemBox',
    'directives/vanishingLabel',
    'directives/editableItem',

    'services/randomizer',
    'services/mailer'
], function () {});
