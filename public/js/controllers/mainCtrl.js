define(['ngapp', 'lodash'], function (ngapp, _) {
    ngapp.controller('MainCtrl', ['$scope', '$http', '$timeout', 'Randomizer', 'Mailer',
        function ($scope, $http, $timeout, Randomizer, Mailer) {
            $scope.$watch('data.people.length', function (newVal) {
                if (newVal < 2) {
                    $scope.data.disableClass = 'disabled';
                } else {
                    $scope.data.disableClass = '';
                }
            });

            $scope.constans = {
                TITLE: 'Christmas Generator',
                DISABLED_CLASS: 'disabled'

            };
            $scope.data = {
                people: [
                    {name: 'piesel@wp.pl'}
                ],
                disableClass: $scope.constans.DISABLED_CLASS,
                tooLittlePeopleError: false

            };

            $scope.addPerson = function (newPersonName) {
                var newPerson = { name: newPersonName },
                    names = _.pluck($scope.data.people, 'name');
                if (newPerson.name && !_.contains(names, newPerson.name)) {
                    newPerson.mailSent = 'null';
                    $scope.data.people.push(newPerson);
                }
            };

            $scope.randomizeGifters = function () {
                if($scope.data.disableClass === '') {

                    $scope.data.disableClass = 'disabled'
                    $timeout(function () {
                        $scope.data.disableClass = ''
                    }, 2000);

                    var gifters = Randomizer.uniqueRandomize($scope.data.people);
                    for(var i = 0, l = $scope.data.people.length; i < l; i++) {
                        $scope.data.people[i].gifter = gifters[i];
                    }

                    _.forEach($scope.data.people, function (person) {
                        person.mailSent = 'progressing';
                        Mailer.sendMail(person.name, person.gifter.name).success(function () {
                            person.mailSent = true;
                        }).error(function () {
                            person.mailSent = false;
                        });
                    });
                } else {
                    $scope.data.tooLittlePeopleError = true;
                }
            };

            $scope.delete = function (person) {
                $scope.data.people = _.without($scope.data.people, person);
            }
        }
    ]);
});
