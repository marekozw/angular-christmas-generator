define(['ngapp', 'lodash'], function (ngapp, _) {
    ngapp.factory('Randomizer', function () {

        var privateUniqueRandomize = function (collection) {

            var myCollection = collection.slice(0),
                selectedItems = [];

            _.forEach(myCollection, function (item) {
                var meRemoved = false,
                    selectedItem = null;

                if (_.contains(myCollection, item)) {
                    myCollection = _.without(myCollection, item);
                    meRemoved = true;
                }

                selectedItem  = _.sample(myCollection);
                selectedItems.push(selectedItem);
                myCollection = _.without(myCollection, selectedItem);

                if (meRemoved) {
                    myCollection.push(item);
                }
            });

            return selectedItems;
        };

        return {
            uniqueRandomize: function (collection) {
                var resultList = [undefined];

                while (_.contains(resultList, undefined)) {
                    resultList = privateUniqueRandomize(collection);
                };
                return resultList;

            }
        };
    });
});
