<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Christmass generator</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/ng-animations.css">
    <link rel="stylesheet" type="text/css" href="css/foundation/css/foundation.min.css">
    <script src="lib/require.js" data-main="js/app"></script>
</head>

<body ng-controller="MainCtrl">

<div>
    <div class="row">
        <img class="columns" src="images/banner.png" alt="banner">
    </div>
    <div class="row">
        <h1 class="columns">{{constans.TITLE}}</h1>
    </div>
    <div class="row">
        <section class="welcome columns">
            <p>Wellcome! Just add e-mail addresses of people that want to take part in
            Christmas gifts drawing and hit <strong>Randomize Gifters</strong>. You need to
            add at least 2 guys.</p>
        </section>
    </div>
    <div class="row">
        <add-item-box
            on-add="addPerson(item)"
            type="email"
            error-message="podaj email"
            placeholder="Type an email"
            >
        </add-item-box>
    </div>
    <div class="mail-list row">
        <ul class="no-bullet">
            <li ng-repeat="person in data.people">
                <editable-item label="person.name" delete-button="true"
                    on-delete="delete(person)" transclusion-alignment="right"
                    type="email" validation-message="Type an Email">
                    <div ng-switch="person.mailSent" class="options">
                        <img ng-switch-when="progressing" heigth="20px" width="20px" src="images/spinner.gif" alt="spinner">
                        <div ng-switch-when="true" class="success radius label padding">Mail sent!</div>
                        <div ng-switch-when="false" class="alert radius label padding">Someth went wrong</div>
                    </div>
                </editable-item>

            </li>
        </ul>
    </div>
    <div class="row">
        <div class="column">
            <vanishing-label text="You need to add at least 2 people"
                timeout="2000" visible="data.tooLittlePeopleError">
            </vanishing-label>
        </div>
    </div>
    <div class="row room-top">
        <div class="column">
            <button class="{{data.disableClass}}" ng-click="randomizeGifters()">Randomize Gifters</button>
        </div>
    </div>
</div>

<!-- <script src="lib/snowstorm-min.js"></script> -->
 <script>
     // snowStorm.snowColor = '#99ccff';
     // snowStorm.flakesMaxActive = 50;
     // snowStorm.useTwinkleEffect = true;
     // //snowStorm.snowCharacter = '@';
 </script>
</body>

</html>
