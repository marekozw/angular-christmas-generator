define(['ngapp'], function (ngapp) {
    ngapp.directive('vanishingLabel', function ($timeout) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'templates/vanishingLabel.html',
            scope: {
                text: '@',
                timeout: '@',
                visible: '='
            },
            link: function (scope, element, attrs) {
                scope.$watch('visible', function (newValue) {
                    if(newValue === true) {
                        $timeout(function () {
                            scope.visible = false;
                        }, scope.timeout);
                    }
                });
            }
        };
    });
});
