require.config({
    paths: {
        'angular': '../lib/angular',
        'ngAnimate':  '../lib/angular-animate',
        'lodash' : '../lib/lodash'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'ngAnimate': {
            deps: ['angular'],
            exports: 'ngAnimate'
        },
        'lodash': {
            exports: 'lodash'
        }
    }
});
require([
        'angular',
        'lodash',
        'ngAnimate',
        'loader'], function (angular, _) {
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['myApp']);
    });
});
