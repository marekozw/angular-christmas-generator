define(['ngapp', 'lodash'], function (ngapp, _) {
    ngapp.factory('Mailer', function ($http) {

        return {
            sendMail: function (email, target) {
                return $http({
                    method: 'POST',
                    url: '/sendMails',
                    data: {
                        email: email,
                        target: target
                    }
                });
            }
        };

    });

});
