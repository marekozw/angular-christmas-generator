define(['ngapp'], function (ngapp) {
    ngapp.directive('editableItem', function ($timeout) {
        return {
            restrict: 'AE',
            replace: true,
            transclude: true,
            templateUrl: 'templates/editableItem.html',
            scope: {
                label: '=',
                deleteButton: '@', // true or false
                transclusionAlignment: '@', // right or left
                onDelete: '&',
                type: '@',
                validationMessage: '@'
            },
            controller: function ($scope) {
                $scope.currentLabel = null;
                $scope.hovered = false;
                $scope.enterHit = true;

                $scope.cancelEdit = function () {
                    if (!$scope.enterHit) {
                        $scope.label = $scope.currentLabel;
                        $scope.enterHit = false;
                    }
                    $scope.disableEditMode();
                };
                $scope.enableEditMode = function () {
                    $scope.editMode = true;
                    $scope.currentLabel = $scope.label;
                };
                $scope.disableEditMode = function () {
                    $scope.editMode = false;
                    $scope.enterHit = false;
                };
                $scope.onKeyPress = function (event) {
                    if (event.which === 13) {
                        if ($scope.label) {
                            $scope.disableEditMode();
                            $scope.enterHit = true;
                        }
                    } else if (event.which === 27) {
                        $scope.cancelEdit();
                    }

                };
            },
            link: function (scope, element) {
                var input = element.find('input')[0];
                scope.setFocus = function () {
                    $timeout(function () {
                       input.focus();
                    });
                }
                scope.$watch('editMode', function () {
                    if (scope.editMode === true) {
                       scope.setFocus();
                    }
                });
            }
        };
    });
});
